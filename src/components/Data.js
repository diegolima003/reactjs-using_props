import React from 'react'

export default function Data() {
    return (
        <p style={{fontSize:'50px', color:'blue', textAlign:'center'}}>
            {new Date().toLocaleDateString()} {/* Com o locale mostra a data no formato local(brasil) */}
        </p>
    )
}