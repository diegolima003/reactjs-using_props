import React from 'react'
import Infos from './Infos'
import Horas from './Horas';
import Data from './Data';



export default function Body() {
    
  const nam = 'John'
  const id = 81
  const adre = 'Calabasas'

    return (
        <>
            <Horas />
            <Data />
            <Infos name={nam} age={id} address={adre}/>
        </>
    )

}