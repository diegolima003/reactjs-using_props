import React from 'react';

export default function Horas() {
    return (
        <p style={{fontSize:'50px', color:'red', textAlign:'center'}}>
            {new Date().toLocaleTimeString()}   {/*método para retorna a hora*/}                 
        </p>
    )
}